document.addEventListener("touchstart", function(){}, true);
document.addEventListener("deviceready", onDeviceReady, false);
var LoopInteger = 0;

function onDeviceReady() {
	SetItem('DeviceUUID',(device.uuid));
	
	if(cordova.plugins.backgroundMode.isActive()==false)
	{
		cordova.plugins.backgroundMode.setEnabled(true);
		
	}		
	
	  
}


		  
$(window).load(function(){
	

  var AppsVersionId = 'UNIKAS Merchant Apps Alpha VER 1';
  var MasterCode = 'UNIKAS';
    SetItem('MasterCode',MasterCode);
  var service_api =  atob(GetItem('ServiceApi'));
	
  $('#full-name').focus();
	
   $('#nextregistration').click(function() {
	  if ($(".invalidinput")[0]) {
		  uniwarning("Kelengkapan Data", "Mohon isi kelengkapan data dengan benar");
		  return false;
	  }
	  
	  
	if ((document.getElementById("full-name").value.length != 0)) {
		if ((document.getElementById("mobile-phone").value.length != 0)) {
			if ((document.getElementById("email-address").value.length != 0)) {
				
					
					
					//start
					
					LoopInteger++;
					ajaxStart(150000);
					var registrationdata = {'Name': $('#full-name').val(),
									 'PhoneNumber': $('#mobile-phone').val(),
									 'Password' : $('#katasandi').val(),
									 'Email' : $('#email-address').val(),
									 'IsContinue' : 0,
									 'MasterCode': MasterCode
					   };
					var postdata = {'data':betoa(btoa(JSON.stringify(registrationdata))),
									'deviceid':GetItem('DeviceUUID')};								

					$.ajax({
						type: "POST",
						data: encap(JSON.stringify(postdata)),
						url: service_api+"registration/create_registration",
						cache: false,
						dataType: "text",
						success: function (data) {
						  refresh_state();
							
						  var response = jQuery.parseJSON(decap(data));	
							//  ssalert(JSON.stringify(response));						  
						  if(response.Status == "Success") 
						  {
						  //check_last_sms();
						  // $(".input-form").attr('disabled',true);
						  ajaxStop();
						  $('#EwalletCode').val(response.Data.EwalletUniqueCode);
						  $('#registration-info').hide();
						  $('#registration-pin').hide();
						  $('#registration-otp').show();
						  $('#otp-code').focus();
						  }
						  else if(response.Status == "Failed1") 
						  {
							  ajaxStop();
							  
								function onConfirm(buttonIndex) 
								{
									if(buttonIndex == 1)
									{
											goToPage("login.html");
									}
									else if (buttonIndex ==2)
									{	
											
											ajaxStart(150000);
												var registrationdata2 = {'Name': $('#full-name').val(),
															 'PhoneNumber': $('#mobile-phone').val(),
															 'Password' : $('#katasandi').val(),
															 'Email' : $('#email-address').val(),
															 'IsContinue' : 1,
															 'MasterCode': MasterCode
												};
												var postdata = {'data':betoa(btoa(JSON.stringify(registrationdata2))),
																'deviceid':GetItem('DeviceUUID')};
																				
												$.ajax({
												type: "POST",
												data: encap(JSON.stringify(postdata)),
												url: service_api+"registration/create_registration",
												cache: false,
												dataType: "text",
												success: function (data) {
												var response = jQuery.parseJSON(decap(data));			
												//alert(JSON.stringify(response));
													 if(response.Status == "Success") 
													{	
														ajaxStop();
														$('#EwalletCode').val(response.Data.EwalletUniqueCode);
														$('#registration-info').hide();
														$('#registration-pin').hide();
														$('#registration-otp').show();
														$('#otp-code').focus();
													}
													else
													{
														ajaxStop();
														uniwarning("Registrasi Gagal", response.Message+", Lupa password ? Silahkan reset password Anda pada halaman login!");
													}							
												},
												error: function (data) {								
													ajaxStop();
													uniwarning("Registrasi Gagal","Silahkan mencoba kembali!");
												},
												complete: function () {
													ajaxStop();								
												}
											});
											
									}												
								}

								navigator.notification.confirm(
									'Apakah Anda ingin melanjutkan Pendaftaran ?', // message
									 onConfirm,            // callback to invoke with index of button pressed
									'Proses Pendaftaran',           // title
									['Tidak','Ya']     // buttonLabels
								);
							  
								//uniwarning('Notifikasi',response.Message);							  
						  }
						  else if(response.Status == "Failed") 
						  {	
							ajaxStop();		
							uniwarning('Notifikasi',response.Message);	
							goToPage('login.html');							
						  }
						},
						error: function (data) {
							refresh_state();
							ajaxStop();
						  uniwarning('Notifikasi','Failed!');
						},
						complete: function () { 
						}
					});
					
					
					//end
					
					
					
					
				
			} else {
				uniwarning("Kelengkapan Data", "Isi Email!");
				$('#email-address').focus();
			}
		} else {
			uniwarning("Kelengkapan Data", "Isi Nomor telepon!");
			$('#mobile-phone').focus();
		}
	} else {
		uniwarning("Kelengkapan Data", "Isi Nama!");
		$('#full-name').focus();
	}
  });
  
  
    $('#mintaotp').click(function() {
	
				ajaxStart(150000);
					var registrationdata2 =	{ 'PhoneNumber': $('#mobile-phone').val(),
										'MasterCode': MasterCode
					   };
					var postdata2 = {'data':betoa(btoa(JSON.stringify(registrationdata2))),
									'deviceid':GetItem('DeviceUUID')};
										
					 $.post(service_api+'registration/generate_otp',encap(JSON.stringify(postdata2))).done(function(data){
						refresh_state();
						var responsesave = jQuery.parseJSON(decap(data));						

						if(responsesave.Status != 'Failed')
						{	ajaxStop();
							uniwarning("Notifikasi", responsesave.Message);
							$('#otp-code').focus();
						}		
						else{
							ajaxStop();
							uniwarning("Notifikasi", responsesave.Message);
							$('#otp-code').focus();
						}
						 
						
					 }).fail(function(data){
						ajaxStop(); 
						uniwarning("Notifikasi", "Minta Kode OTP gagal!");
						$('#otp-code').focus();						 
					 });
	});
  
  /*
  	window.sms.isSupported ((function(supported) {
				if(supported)
				{
					  window.sms.startReceiving (function(msg) {
						  
							 var res = msg.toLowerCase();
					 
							if(res.indexOf("unikas") >= 0)
							{						
								if( res.indexOf(".") >= 0)
								{
								// Found world
								var check = res.indexOf(".");
								var getOTPCode = res.substring(check-6, check);				
								$('#otp-code').val(getOTPCode);
								ajaxStop();
								}
							}
					  }, function() {
						  alert("Error while receiving messages");
					  });          
				 } else {
					 alert("SMS not supported");
				 }

		  }), function() {
				  alert("Error while checking the SMS permission ");
		  });
		*/  
	var last_sms_id = 0;
	var last_sms_sequence = '';
	
	//get_sms();
	// SMS LISTEN
	
	function get_sms(){
			if(SMS)SMS.listSMS({}, function(data){
				
				last_sms_id = data[0]._id;
				last_sms_sequence =  data[0].sequence_time;
    			
    			
    			
        		
        	}, function(err){
        		updateStatus('error list sms: ' + err);
        	});
		
	}
	
	
	function check_last_sms(){
		var refreshIntervalId = setInterval(function(){
			//console.log('check_last_sms');
			if(SMS)SMS.listSMS({}, function(data){
				var message = data[0].body;
				var last_sms = data[0]._id;
				var last_sequence =  data[0].sequence_time;
    			if((last_sms > last_sms_id) && (last_sequence > last_sms_sequence)){
					
					if(message.includes("UNIKAS")){
						
						var otp = message.substring(37, message.indexOf('.'));
						
						last_sms_id = data[0]._id;
						last_sms_sequence =  data[0].sequence_time;
						$('#otp').val(otp);
						
						clearInterval(refreshIntervalId);
					}
	
				}

					}, function(err){
					
					});
			
				}, 2000);
	
	}
    
	var generalmata = 0;
	$("#matamu").click(function() {
		if(generalmata == 0)
		{
			$("#pin-code").attr('type','text');
			$(this).attr('src','images/hide-eye.png');
			generalmata = 1;
		}
		else{
			$("#pin-code").attr('type','Password');
			$(this).attr('src','images/view-eye.png');
			generalmata = 0;
		}

	});
  
  $('#submitregistration').click(function(){
	  if ($(".invalidinput")[0]) {
		  uniwarning("Kelengkapan Data", "Mohon isi kelengkapan data dengan benar");
		  return false;
	  }
	  
	  if (document.getElementById("otp-code").value.length >= 6) {
			LoopInteger++;
			ajaxStart(150000);
			 var otpvalid = {'PhoneNumber': $('#mobile-phone').val(),
								 'OTPCode': $('#otp-code').val(),
								 'MasterCode': MasterCode
				   };    
			
			 var postdata = {'data':betoa(btoa(JSON.stringify(otpvalid))),
							'deviceid':GetItem('DeviceUUID')};			 			 

			 $.ajax({
				type: "POST",
				data: encap(JSON.stringify(postdata)),
				url: service_api+"registration/is_otp_valid_pos",
				cache: false,
				dataType: "text",
				success: function (data) {
				refresh_state();
				ajaxStop();
				var response = jQuery.parseJSON(decap(data));			
				//alert(JSON.stringify(response));
				if(response.Status == "Success") 
				{				 
				  $('#registration-info').hide();
				  $('#registration-pin').show();
				  $('#registration-otp').hide();
				  $('#pin-code').focus();
				}
                else if(response.Status == "Failed")
                {
                    uniwarning('Login Gagal', response.Message);
					$('#otp-code').val('');
					$('#otp-code').focus();
                }
				else
				{	
					uniwarning('Login Gagal', response.Message);
                    goToPage('login.html');
				}
				},
				error: function (data) {
					refresh_state();
					//alert(JSON.stringify(data));
					ajaxStop();
				  uniwarning("Notifikasi","Failed!");
				},
				complete: function () { 
				}
			});
	  } else {
		  uniwarning("Kelengkapan Data","Isi Kode OTP!");
		  $('#otp-code').val('');
		  $('#otp-code').focus();
	  }
    

  });


  $('#finishregistration').click(function(){
	  if (document.getElementById("pin-code").value.length >= 6) 
	  {
			LoopInteger++;
			$('#registration-info').hide();
			$('#registration-pin').hide();
			$('#registration-otp').hide();
			$('#registration-toc').show();
			
	  } else {
		  uniwarning("Kelengkapan Data","Isi PIN minimal 6 karakter!");
		  $('#pin-code').val('');
		  $('#pin-code').focus();
	  }
  });
  
  
   $('#finalfinishregistration').click(function(){
		if (document.getElementById("checkboxRegistration").checked == true)
		{
			ajaxStart(150000);
			 var pindata = {'EwalletUniqueCode': $('#EwalletCode').val(),
								 'PINCode': $('#pin-code').val(),
								 'DeviceUUID': GetItem('DeviceUUID'),
								 'MasterCode': MasterCode
				   };    
			
			 var postdata = {'data':betoa(btoa(JSON.stringify(pindata))),
							'deviceid':GetItem('DeviceUUID')};
			 			 	

			 $.ajax({
				type: "POST",
				data: encap(JSON.stringify(postdata)),
				url: service_api+"registration/update_pin",
				cache: false,
				dataType: "text",
				success: function (data) {
				  refresh_state();
				  ajaxStop();
				  uniwarning("Registrasi Sukses","Selamat Anda telah bergabung dengan Nobu EPAY!");
				  goToPage("login.html");
				},
				error: function (data) {
					refresh_state();
					ajaxStop();
				  uniwarning("Registrasi Gagal","Silahkan mencoba kembali!");
				},
				complete: function () {
					ajaxStop();
				}
			});
		}
		else
		{
			uniwarning("Kelengkapan Data","Setujui Syarat dan Ketentuan!");
		}
		
	});

	
	goToPage = function(hrefPage){
      document.location = hrefPage;
    };
   
	$('#registerback').click(function(){
		var count = 0;
		count++;
		if(LoopInteger == 1)
		{	
			if(count == 2)
			{
			   goToPage("login.html");
			}
			else
			{			
			$('#registration-info').show();
			$('#registration-pin').hide();
			$('#registration-otp').hide();
			$('#registration-toc').hide();
			}
		}
		else if(LoopInteger == 2)
		{
			$('#registration-info').hide();
			$('#registration-pin').hide();
			$('#registration-otp').show();
			$('#registration-toc').hide();
		}
		else if(LoopInteger == 3)
		{
			$('#registration-info').hide();
			$('#registration-pin').show();
			$('#registration-otp').hide();
			$('#registration-toc').hide();
		}
		else
		{
			goToPage("login.html");
		}
		
		LoopInteger--;	
		/*
		if(LoopInteger >= 0)
		{   
		   if(LoopInteger != 4)
		   {				
				function onConfirm(buttonIndex) {
					if(buttonIndex == 1)
					{
						//navigator.app.exitApp();
					}
					else if (buttonIndex ==2)
					{	
						
						ajaxStart(150000);
						 var pindata = {	'UserId': $('#mobile-phone').val(),
											'MasterCode': MasterCode
							   };    
						
						 var postdata = {'data':btoa(JSON.stringify(pindata))};

						 $.ajax({
							type: "POST",
							data: postdata,
							url: service_api+"registration/cancel_Registration",
							cache: false,
							dataType: "text",
							success: function (data) {
							var response = jQuery.parseJSON(atob(data));			
							//alert(response);
								if(response == true)
								{	
									ajaxStop();
									uniwarning("Registrasi Gagal", "Anda melakukan batal registrasi!");
									goToPage("login.html");
								}
								else
								{
									ajaxStop();
									uniwarning("Cancel Registrasi Gagal","Silahkan mencoba kembali!");
								}							
							},
							error: function (data) {								
								ajaxStop();
								uniwarning("Cancel Registrasi Gagal","Silahkan mencoba kembali!");
							},
							complete: function () {
								ajaxStop();								
							}
						});
						
					}
								
				}

				navigator.notification.confirm(
					'Apakah ingin membatalkan Pendaftaran ?', // message
					 onConfirm,            // callback to invoke with index of button pressed
					'Proses Daftar',           // title
					['Tidak','Ya']     // buttonLabels
				);
		   }
		   else
		   {
			   
		   }
		}
		*/
		
	});

	
	/*
    document.addEventListener("backbutton", function(e){
		var count = 0;
		count++;
		if(LoopInteger == 1)
		{	
			if(count == 2)
			{
			   goToPage("login.html");
			}
			else
			{			
			$('#registration-info').show();
			$('#registration-pin').hide();
			$('#registration-otp').hide();
			$('#registration-toc').hide();
			}
		}
		else if(LoopInteger == 2)
		{
			$('#registration-info').hide();
			$('#registration-pin').hide();
			$('#registration-otp').show();
			$('#registration-toc').hide();
		}
		else if(LoopInteger == 3)
		{
			$('#registration-info').hide();
			$('#registration-pin').show();
			$('#registration-otp').hide();
			$('#registration-toc').hide();
		}
		else
		{
			goToPage("login.html");
		}
		
		LoopInteger--;
	
    }, false);
	*/
    
	numberformat = function(obj) {
		if (!/^\d+$/.test(obj.value)) {
		obj.classList.add("invalidinput");
		} else {
			obj.classList.remove("invalidinput");
		}
	}
	
	phoneformat = function(obj) {
		if (!/^\d+$/.test(obj.value)) {
		obj.classList.add("invalidinput");
		} else {
			obj.classList.remove("invalidinput");
		}
	}	
	
	emailvalidation = function(obj) {
		if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(obj.value)) {
			uniwarning("Kelengkapan Data", "Format email tidak valid");
			obj.value = "";
			obj.focus();
		}
	}

   
});