document.addEventListener("backbutton", onBackKeyDown, false);
document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("touchstart", function(){}, true);
document.addEventListener("pause", onPause, false);
document.addEventListener("resume", onResume, false);


function onDeviceReady() {
		
		window.plugins.sim.getSimInfo(successCallback, errorCallback);
		SetItem('DeviceUUID',device.uuid);
		SetItem('Device',JSON.stringify(device));
		document.cookie = btoa(JSON.stringify(device));
		var tests = btoa(JSON.stringify(device));
		SetItem('DeviceInformation',tests);
		SetItem('LocationInformation',btoa(GetItem('UnikasLocationData')));
				
		if(GetItem('UnikasWalletApp')!="null")
		{	
			checkConnection();
			var userdata = JSON.parse(atob(GetItem('UnikasWalletApp')));
			var appdata = {service:GetItem('MasterCode'),
						EWalletUniqueCode:userdata.EwalletUniqueCode,
						DeviceData:device};				
		
			socket.emit('send_notification',{
							"EWalletUniqueCode": appdata.EWalletUniqueCode,
							"AccountType": userdata.AccountType,
							"DeviceUUID": device.uuid
						});
							
		}		
			
				
			cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
				for (var permission in statuses){
					switch(statuses[permission]){
						case cordova.plugins.diagnostic.permissionStatus.GRANTED:
							//console.log("Permission granted to use "+permission);
							break;
						case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
							//console.log("Permission to use "+permission+" has not been requested yet");
							break;
						case cordova.plugins.diagnostic.permissionStatus.DENIED:
							//console.log("Permission denied to use "+permission+" - ask again?");
							break;
						case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
							//console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
							break;
					}
				}
			}, function(error){
				console.error("The following error occurred: "+error);
			},[		
				cordova.plugins.diagnostic.permission.READ_PHONE_STATE			
			]);						
																	
}


function successCallback(result) {
	SetItem('SIMSerialNumber',result.simSerialNumber);
	//alert("suk"+result.simSerialNumber);
	//localStorage.setItem('SIMSerialNumber2',result.simSerialNumber);
  //console.log(result);
}

function errorCallback(error) {
	alert("SIM Card tidak terdeteksi, "+error);
  //console.log(error);
}

// Android only: check permission
function hasReadPermission() {
  window.plugins.sim.hasReadPermission(successCallback, errorCallback);
}

// Android only: request permission
function requestReadPermission() {
  window.plugins.sim.requestReadPermission(successCallback, errorCallback);
}

function checkConnection() {
	var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    //alert('Connection type: ' + states[networkState]);

	if(states[networkState] == 'No network connection')
	{
						
		goToPage('no_connection.html');
	
	}
	
}

function onPause() {
	
	if(GetItem('UnikasWalletApp') != "null")
    {	

	}
	/*	
		//refresh_state();
		//check_auth();
		var pagenow = GetItem('ButtonLoadPage');
		//alert("Pagenow"+pagenow);
		if(pagenow != 'pluginactive')
		{
			//alert("pauselock");
			screen_lock_funct();
		}

		
		//if(pagenow != 'pluginactive' && pagenow != 'null' && pagenow != null)
		///{	
//			screen_lock_funct();
		//}
	}
	
	
	if(GetItem('UnikasWalletApp') != "null")
    {	
			
		var pagenow = GetItem('ButtonLoadPage');
		//alert("Pagenow"+pagenow);
		if(pagenow != 'pluginactive')
		{	
			SetItem('ButtonLoadPage', null);
		}
		else
		{
			SetItem('ButtonLoadPage', 'pluginactive');
		}

	}
	*/
}

function onResume() {
	
	
	if(GetItem('UnikasWalletApp') != "null")
    {	
		//check_auth();
		
		var pagenow = GetItem('ButtonLoadPage');
		var pagenow2 = GetItem('ButtonLoadPageMarshmallow');
		
		//alert("pagenow"+pagenow);
		//alert("pagenow2"+pagenow2);
		
		if(pagenow == 'pluginactive' && pagenow2 == 'pluginactive')
		{
			//alert("Go to Screen Lock");
			//goToPage('screen_lock.html');
			//SetItem('ButtonLoadPage', null);
			//SetItem('ButtonLoadPageMarshmallow', null);
		}
		
		/*
		var pagenow = GetItem('ButtonLoadPage');
		var pagenow2 = GetItem('ButtonLoadPageMarshmallow');
		
		
		//alert("pagenow"+pagenow);
		//alert("pagenow2"+pagenow2);
		var appsclose = GetItem('AppsClose');
			//alert("appsclose"+appsclose);
			if(appsclose == 'true')
			{	
				SetItem('AppsClose', null);
				goToPage('screen_lock.html');
			}		
			
		if(pagenow != 'pluginactive' && pagenow2 != 'pluginactive')
		{
			//alert("screen_lock");
			goToPage('screen_lock.html');
		}
		else if(pagenow == 'pluginactive' && pagenow2 == 'pluginactive')
		{
			//alert("nullin");
			//Menandai agar saat pause app tidak terlock
			SetItem('ButtonLoadPage', null);
			SetItem('ButtonLoadPageMarshmallow', null);
		}
		*/
	}
	else
	{	
		/*
		SetItem('ButtonLoadPage', 'pluginactive');
		SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
		*/
		//goToPage('login.html');
	}
	
}


var smsList = [];
var interceptEnabled = false;
var loop = 0;

function OutTimer() {    
    navigator.app.exitApp();	
}

function onBackKeyDown() {
	
	var loadpage = GetItem('PageLoadOnBack');
	
	if(loadpage == 'wallet/merchant_page'){
	    goToPage('wallet.html');
	}
	else{
		//Trigger
		//loop = loop + 1;
		//if(loop == 1)
		//{
		//uniwarning('Keluar', "Tekan sekali lagi!");
		//}
		//else
		//{
//			 navigator.app.exitApp();
		//}	
        
        function onConfirm(buttonIndex) {
					if(buttonIndex == 1)
					{
						
					}
					else if (buttonIndex ==2)
					{	
						//SetItem('ButtonLoadPage', null);
						//SetItem('ButtonLoadPageMarshmallow', null);
						SetItem('AppsClose', 'true');
                        navigator.app.exitApp();	                     				
					}
        }
        navigator.notification.confirm(
					'Apakah anda ingin menutup Aplikasi ?', // message
					 onConfirm,            // callback to invoke with index of button pressed
					'Keluar',           // title
					['Tidak','Ya']     // buttonLabels
				);
	}
}

var generalmata = 0;
$("#matamu").click(function() {
	if(generalmata == 0)
	{
	    $("#password-field").attr('type','text');
	    $(this).attr('src','images/hide-eye.png');
		generalmata = 1;
	}
	else{
		$("#password-field").attr('type','Password');
		$(this).attr('src','images/view-eye.png');
		generalmata = 0;
	}

});


/*
function onBackKeyDown() {
	goToPage('wallet.html');
}
*/


$(function() {
    'use strict';
	
	var buffer_page = null;
	/*
	 if(GetItem('UnikasWalletApp')!="null")
    {       	
			var appsclose = GetItem('AppsClose');
			//alert("appsclose"+appsclose);
			if(appsclose == 'true')
			{
				goToPage('screen_lock.html');
			}		
    }
	*/
	
	// alert(GetItem('SIMSerialNumber'));
		
    
	$("#login-submit").click(function(event) {

		
		// uniwarning('fsdf',GetItem('SIMSerialNumber'));
		event.preventDefault();
		
		////console.log(GetItem('DeviceUUID'));
		if(document.getElementById("userid-field").value.length != 0)
		{
			if(document.getElementById("password-field").value.length >= 6)
			{
				var logindata = {'UserId':$("#userid-field").val(),
								 'Password':$("#password-field").val(),
								 'DeviceUUID':GetItem('DeviceUUID'),
								 'SIMSerialNumber':GetItem('SIMSerialNumber'),
								 'DeviceInformation': atob(GetItem('DeviceInformation')),
								 'LocationInformation': atob(GetItem('LocationInformation'))
								 };	
				
				login(logindata);
			}
			else
			{
					navigator.notification.alert(
							'Isi kata sandi (minimal 6 digit)!',  // message
							null,         // callback
							'Kelengkapan Data',            // title
							'Tutup'                  // buttonName
						);
					$("#password-field").val('');
					$("#password-field").focus();
			}
		}
		else
		{
				navigator.notification.alert(
						'Isi Nomor Telpon/ Email!',  // message
						null,         // callback
						'Kelengkapan Data',            // title
						'Tutup'                  // buttonName
					);
				$("#userid-field").val('');
				$("#userid-field").focus();
		}

    });
	
	/*
	$("#login-form").submit(function(event) {
	   
		event.preventDefault();
		
		//console.log(GetItem('DeviceUUID'));
        var logindata = {'UserId':$("#userid-field").val(),
						 'Password':$("#password-field").val(),
						 'DeviceUUID':GetItem('DeviceUUID')};	
		
		//console.log(logindata);
		login(logindata);
		

    }); */
	
	$("#login-submitotp").click(function(event) {	
		event.preventDefault();
					
		////console.log(GetItem('DeviceUUID')); 
						 
		var logindata = {'UserId':$("#userid-field").val(),
								 'Password':$("#password-field").val(),
								 'DeviceUUID':GetItem('DeviceUUID'),
								 'SIMSerialNumber':GetItem('SIMSerialNumber'),
								 'OTPCode':$("#kodeotp-field").val(),
								 'DeviceInformation': atob(GetItem('DeviceInformation')),
								 'LocationInformation': atob(GetItem('LocationInformation'))
								 };	
		
		////console.log(logindata);
		loginandotp(logindata);

    });
	
	$("#press_back").click(function(event) {	
		 goToPage('login.html');
    });
	
	/*
	$("#login-formotp").submit(function(event) {
	   
		event.preventDefault();
					
		////console.log(GetItem('DeviceUUID'));
        var logindata = {'UserId':$("#userid-field").val(),
						 'Password':$("#password-field").val(),
						 'OTPCode':$("#kodeotp-field").val(),
						 'DeviceUUID':GetItem('DeviceUUID')};	
		
		//console.log(logindata);
		loginandotp(logindata);
		

    });
	*/
	
	$("#forgot_password").click(function(event) {


		buffer_page = $('.w-form').html;
        
	   var ServiceApi = atob(GetItem('ServiceApi'));
		$.get(ServiceApi+'base/page/?type=wallet/reset_pasword').done(function(data){

			$('.w-form').empty().append(data);

		});


    });

	
	
	
   $('.loading-mask').addClass('stop-loading');
	
	$('#logout-btn').click(function(){
		//logout();
		
		 function onConfirm(buttonIndex) {
					if(buttonIndex == 1)
					{
						
					}
					else if (buttonIndex ==2)
					{	
                        clearapp();
						goToPage('index.html');                 				
					}
        }
        navigator.notification.confirm(
					'Apakah anda ingin keluar dari Aplikasi ?', // message
					 onConfirm,            // callback to invoke with index of button pressed
					'Keluar',           // title
					['Tidak','Ya']     // buttonLabels
				);
				
		
		
	});
	
	

	$('body').on('click', function(e){
      var pagePathName= window.location.pathname;
      var currentpage = pagePathName.substring(pagePathName.lastIndexOf("/") + 1);

       // console.log('Current Page = '+currentpage);
		if(currentpage == 'wallet.html'){

				SetItem('LastAccessTime',$.now());
				//alert($.now());
				//alert("body on click");
		
		  }
        //return false;
    });
	

	$('body .linkrequest').on('click', function(e){
      var pagePathName= window.location.pathname;
      var currentpage = pagePathName.substring(pagePathName.lastIndexOf("/") + 1);

       // console.log('Current Page = '+currentpage);
		if(currentpage == 'wallet.html'){

				SetItem('LastAccessTime',$.now());
		
		  }


	    load_page($(this));
	    socket.emit('refresh',device);	
	   
	
        //return false;
    });
    
	
	
	$('body .button-home').on('click', function(e){
      
   
	

      e.preventDefault();
	  goToPage('wallet.html');
         socket.emit('refresh',device);	
	   
	 

    });
	

	
   window.onpopstate = function(e){
        $('.loading-mask').addClass('stop-loading');
    };
	
   $(".btn-close-button").on('click', function(e){
    	$('.wrapper-mask').trigger('click');
      	e.preventDefault();

        return false;
    });

   $('.add-buton').click(function(){

   		var url;
   });

   // Scan Qr Code 

 $('.scantopay').click(function()
   {
	

	
	  	//uniwarning('Keamanan', "Aplikasi menutup aplikasi, Scanner kita kunci!");
		//goToPage('screen_lock.html');
	 SetItem('CurrentQrcodeData',null);
	 //Menandai agar saat pause app tidak terlock		
	SetItem('ButtonLoadPageMarshmallow', 'pluginactive');	
	 //SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
	 
	 cordova.plugins.barcodeScanner.scan(
		function (result) {//console.log(JSON.stringify(result));
			if(!result.cancelled)
			{
				var urlscan = result.text;
								   var qrdata = urlscan.split("=");
								   var qrcode = qrdata[1];								  
								   SetItem('CurrentQrcodeData',qrcode);
															  
								   var service_api =  atob(GetItem('ServiceApi'));
								   $(".navbar-title").text('Pembayaran');
															   
								   var page_api = service_api+'base/page/?type=wallet/payment_qr';
								   $('.loading-mask').removeClass('stop-loading');
								   $.get(page_api).done(function(data){
									   $('.loading-mask').addClass('stop-loading');
									   $('.body').empty().append(data);
										
								   }).fail(function(){
									   $('.loading-mask').addClass('stop-loading');
									   alert('Communication to server fail');
									   //alert('Cbambio');
								   });
							
			} else{goToPage('wallet.html');}
		
		}, function (error) {
			  alert("Scanning failed: " + error);
		  }
	);
	
	
   });
   
  // upload picture
	
	
	var pictureSource;   // picture source
    var destinationType; // sets the format of returned value
	var documenttype;
	var selector; 
	
	
	 $('.profile-avatar-image').click(function(event){
		
		//Menandai agar saat pause app tidak terlock
		//SetItem('ButtonLoadPage', 'pluginactive');
		//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
		
		
		function onDeviceReady() 
		{
		//Menandai agar saat pause app tidak terlock
		//SetItem('ButtonLoadPage', 'pluginactive');
		//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
			
		//if(cordova.plugins.backgroundMode.isActive()==false)
//			{	

			//Menandai agar saat pause app tidak terlock
			//SetItem('ButtonLoadPage', 'pluginactive');
			//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
		
			//cordova.plugins.backgroundMode.setDefaults({ silent: true });
			//cordova.plugins.backgroundMode.setEnabled(true);					
						
		
			cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
				for (var permission in statuses){
					switch(statuses[permission]){
						case cordova.plugins.diagnostic.permissionStatus.GRANTED:
							//console.log("Permission granted to use "+permission);
							break;
						case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
							//console.log("Permission to use "+permission+" has not been requested yet");
							break;
						case cordova.plugins.diagnostic.permissionStatus.DENIED:
							//console.log("Permission denied to use "+permission+" - ask again?");
							break;
						case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
							//console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
							break;
					}
				}
			}, function(error){
				console.error("The following error occurred: "+error);
			},[
				cordova.plugins.diagnostic.permission.READ_EXTERNAL_STORAGE,
				cordova.plugins.diagnostic.permission.CAMERA
			]);														
			//}			
		}
			
	
		 event.preventDefault();

		  pictureSource=navigator.camera.PictureSourceType;
		  destinationType=navigator.camera.DestinationType;
	 	  documenttype = $(this).attr('document-type');
		  selector = $(this);

	 	  var url_link = $(this)[0].src;
		 // alert(url_link);
	 	  if(url_link != null || url_link != '')
		  {
			  var btn = "button";
			
			swal({
			  title: "",
			   text: 
				'<button style="background-color:#030f48" type="button" id="btnGaleri" >File Galeri</button> ' +
				'<button style="background-color:#030f48" type="button" id="btnAmbilFoto" >Ambil Foto</button>',
				html: true,
				imageUrl: url_link,
				imageSize: '250x250',
				showConfirmButton: false,
				showCancelButton: true,			  
			},
			function(){
				
			});	
			}
			else{
					var capture = navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, allowEdit: true,
					targetWidth: 500, targetHeight: 500,
					destinationType: destinationType.DATA_URL });
			}		
						
			
	 });
	 
	 	$(document).on('click', "#btnGaleri", function() 
		{
			  //Menandai agar saat pause app tidak terlock
		      //SetItem('ButtonLoadPage', 'pluginactive');
			  //SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
			  
			  swal.close();
			  getPhoto(pictureSource.SAVEDPHOTOALBUM);
		});

		$(document).on('click', "#btnAmbilFoto", function() 
		{	
			
			  //Menandai agar saat pause app tidak terlock
		      //SetItem('ButtonLoadPage', 'pluginactive');		
			  //SetItem('ButtonLoadPageMarshmallow', 'pluginactive');	
			  
			  swal.close();
				
				 capturePhotoEdit2();
				//navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20,  allowEdit: true, correctOrientation: true,
				//targetWidth: 500, targetHeight: 500,
				//destinationType: destinationType.DATA_URL });
		});
		
		
		function capturePhotoEdit2() 
	{

      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
			var capture = navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true, correctOrientation: true,
				targetWidth: 500, targetHeight: 500,
				destinationType: destinationType.DATA_URL });

    }

			
	  function onPhotoDataSuccess(imageData) {
		
		//SetItem('ButtonLoadPage', 'pluginactive');		
		//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');		
			  
      // Uncomment to view the base64-encoded image data
	  var service_api = atob(GetItem('ServiceApi'));
       var postdata = btoa(JSON.stringify({'Image':imageData}));
		 $.post(service_api+'base/upload',encap(JSON.stringify({'data':betoa(postdata), 'deviceid':GetItem('DeviceUUID')}))).done(function(data){
			 
			 refresh_state();
             var response = jQuery.parseJSON(decap(data));			 
		
			 if(response.Status != 'Failed')
				{
     				
					var img_url = response.Data;
					selector.attr('src',img_url);
					
					
					// Save 
					//console.log(img_url);
					var UserId = atob(GetItem('UserId'));
					var postsave = btoa(JSON.stringify({'photourl':img_url, 'userid' : UserId}));
																
					$.post(service_api+'base/update_photo',encap(JSON.stringify({'data':betoa(postsave), 'deviceid':GetItem('DeviceUUID')}))).done(function(data){
						refresh_state();
						var responsesave = jQuery.parseJSON(decap(data));
						if(responsesave.Status != 'Failed')
						{
			     			// alert(responsesave.Status);
			     			SetItem('PhotoProfile', btoa(img_url));
						}		
						else{
							// alert(responsesave.Status);
						}
						 
						
					 }).fail(function(){
						 alert('Failed To upload data');
						 
					 });
				
				
				}		
			else{
				alert(response.Status);
			}
			 
			
		 }).fail(function(){
			 alert('Failed To upload data');
			 
		 });
		}
	
	// Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
      // Uncomment to view the image file URI
      // //console.log(imageURI);

      // Get image handle
      // 
      var largeImage = document.getElementById('profileImage');

      // Unhide image elements
      //
//      largeImage.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      //
      largeImage.src = imageURI;
	  
	var service_api = atob(GetItem('ServiceApi'));
	   var postdata = btoa(JSON.stringify({'Image':imageURI}));
		 $.post(service_api+'base/upload',encap(JSON.stringify({'data':betoa(postdata), 'deviceid':GetItem('DeviceUUID')}))).done(function(data){
			 refresh_state();
             var response = jQuery.parseJSON(decap(data));			 
		
			 if(response.Status != 'Failed')
				{
     				
					var img_url = response.Data;
					selector.attr('src',img_url);
					
					
					// Save 
					//console.log(img_url);
					var UserId = atob(GetItem('UserId'));
					var postsave = btoa(JSON.stringify({'photourl':img_url, 'userid' : UserId}));
													
					$.post(service_api+'base/update_photo',encap(JSON.stringify({'data':betoa(postsave), 'deviceid':GetItem('DeviceUUID')}))).done(function(data){
						refresh_state();
						var responsesave = jQuery.parseJSON(decap(data));
						if(responsesave.Status != 'Failed')
						{
			     			// alert(responsesave.Status);
			     			SetItem('PhotoProfile', btoa(img_url));
						}		
						else{
							// alert(responsesave.Status);
						}
						 
						
					 }).fail(function(){
						 alert('Failed To upload data');
						 
					 });
				
				
				}		
			else{
				alert(response.Status);
			}
			 
			
		 }).fail(function(){
			 alert('Failed To upload data');
			 
		 });
    }
	 // A button will call this function
    //
    function getPhoto(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, {  quality: 20, allowEdit: true, correctOrientation: true,
        targetWidth: 500, targetHeight: 500,
		destinationType: destinationType.DATA_URL,
        sourceType: source });
    }
	
	
	function onFail(message) {
		   //SetItem('ButtonLoadPage', 'pluginactive');	
		   //SetItem('ButtonLoadPageMarshmallow', 'pluginactive');			  
      return false;
    }
	
	
	var inisialBtnLihatPin = 0;
	$("#btnLihatPin").click(function() {
		if(inisialBtnLihatPin == 0)
		{
			$("#pinScreenLock").attr('type','text');
			$(this).attr('src','images/hide-eye.png');
			inisialBtnLihatPin = 1;
		}
		else{
			$("#pinScreenLock").attr('type','Password');
			$(this).attr('src','images/view-eye.png');
			inisialBtnLihatPin = 0;
		}

	});
	
	/*
	//var pin = (+!![] + []) + (!+[] + !![] + []) + (!+[] + !![] + !![] + []) + (!+[] + !![] + !![] + !![] + []) + (!+[] + !![] + !![] + !![] + !![] + []) + (!+[] + !![] + !![] + !![] + !![] + !![] + []);
    var enterCode = "";
    enterCode.toString();
	
	//$("#pinScreenLock").val('');
	//$("#pinScreenLock").focus();
	  
    $(".numberscreenlock").click(function() {
		
      var clickedNumber = $(this).text();
      enterCode = enterCode + clickedNumber;
      var lengthCode = parseInt(enterCode.length);
      lengthCode--;
      //$("#fields .numberfield:eq(" + lengthCode + ")").addClass("active");	 
	  $("#pinScreenLock").val(enterCode);
      //alert(lengthCode);
	  if (lengthCode == 5) {									
		  loginpin(enterCode);	
      }

    });
    
	$("#btnDeleteScreenLock").click(function(){
      enterCode = "";
	   $("#pinScreenLock").val('');
	   //$("#pinScreenLock").focus();
	  //alert(enterCode);
      //$("#fields .numberfield").removeClass("active");
      //$("#fields .numberfield").removeClass("right");
      //$("#numbers").removeClass("hide");
    });
	*/
	
});


$(window).load(function(){


	var device = JSON.parse(GetItem('Device'));	
    
	
	// Realtime Socket 
	if(GetItem('UnikasWalletApp') && GetItem('UnikasWalletApp') != 'null' )
	{
	
    setInterval(function(){
        refreshdata();
        
    },30000);    
        
	//console.log(device);
	var userdata = JSON.parse(atob(GetItem('UnikasWalletApp')));
	
					
	var appdata = {service:'UNIKAS',
					EWalletUniqueCode:userdata.EwalletUniqueCode,
                  Device:device};				
	
	socket.on('connect',function(){
		
				socket.emit('connect_service',appdata);
			});
			
	socket.on('message',function(data){
				//console.log(data);
			});
			
	socket.on('notify',function(data){
				
				swal(data);
				
			});
        
     socket.on('refreshdata',function(data){
			
                var sdata = JSON.parse(atob(data));
                if(localStorage.getItem('DeviceUUID') == sdata.DeviceId){                    
                    localStorage.setItem(sdata.DeviceId,sdata.Token);                   
                }
     
                key = sdata.Token; 
     
                
    
			}); 

	
	socket.on('closed_scanned',function(data){
			
			if(appdata.EWalletUniqueCode == data.EwalletUniqueCode){
				goToPage('wallet.html');
			}
		});

	socket.on('get_quotation',function(data){
			if(appdata.EWalletUniqueCode == data.ReceiverEwalletUniqueCode) {
					 SetItem('CurrentQrcodeData',null);

							   
				//console.log(data);			   
							   var qrcode = data.QRCodeVal;	
							   
							   SetItem('CurrentQrcodeData',qrcode);
							
							   var service_api =  atob(GetItem(ServiceApi));
							   $(".navbar-title").text('Pembayaran');
							   
							   
			var page_api = service_api+'base/page/?type=wallet/payment_qr';
							   $('.loading-mask').removeClass('stop-loading');
							   $.get(page_api).done(function(data){
								   $('.loading-mask').addClass('stop-loading');
								   $('.body').empty().append(data);
									
							   }).fail(function(){
								   $('.loading-mask').addClass('stop-loading');
								   alert('Communication to server fail');
							   });
							   
		
		
			var now = new Date().getTime(),_0_seconds_from_now = new Date(now + 1000);		
			window.plugin.notification.local.schedule({
							id:      1,
							title:   'UNIKAS',
							message: data.Message,					
							date:    _0_seconds_from_now,
							icon: 'res://drawable/unikas_logo.png'
						});
						
			cordova.plugins.notification.local.on("click", function (notification) {
								$("#btnHistory" ).trigger( "click" );
					
			});
		                
		                
		         
			 
			}
		});
        
     socket.on('refreshdata',function(data){
			
                var sdata = JSON.parse(atob(data));
                if(GetItem('DeviceUUID') == sdata.DeviceId){                    
                    SetItem(sdata.DeviceId,sdata.Token);                   
                }
     
                key = sdata.Token; 
     
                
    
			}); 


	
	socket.on('get_notify',function(data){
		
		//uniwarning('Keamanan', "Aplikasi kami close!");
		if(appdata.EWalletUniqueCode == data.ReceiverEwalletUniqueCode) {
			updatebalance();
			$("#btnHistory" ).trigger( "click" );
			var now = new Date().getTime(),_0_seconds_from_now = new Date(now + 1000);		
			window.plugin.notification.local.schedule({
							id:      1,
							title:   'UNIKAS',
							message: data.Message,					
							date:    _0_seconds_from_now,
							icon: 'res://drawable/unikas_logo.png'				
						});
						
			
						
			cordova.plugins.notification.local.on("click", function (notification) {
					$("#btnHistory" ).trigger( "click" );
					
			});

		}				
		else if( (appdata.EWalletUniqueCode == data.EWalletUniqueCode) && (data.AccountType == 'USERS' || data.AccountType == 'MERCHANT') )		
		{		
			//Jika Deviceuuidnya berbeda akan logout otomatis
			if(GetItem('DeviceUUID') != data.DeviceUUID)
			{
			uniwarning('Keamanan', "Aplikasi kami tutup!");
			clearapp();
			goToPage('index.html');
			}
		}
	});
	
				

	}
    else{
   // //console.log(device);
        
    
   // console.log('NO '+GetItem('UnikasWalletApp'));
        
  			
	
	socket.on('connect',function(){
		  var adata = {service:'UNIKAS',
					EWalletUniqueCode:GetItem('DeviceUUID'),
                  Device:device};	
				socket.emit('connect_service',adata);
			});
         
    }
   

});







