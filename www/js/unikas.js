   document.addEventListener("deviceready",onDeviceReady,false);

	var pictureSource;   // picture source
    var destinationType; // sets the format of returned value

	var merchantType;
	
    function onDeviceReady() {

        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType; 
        
    
        init();
        secure_check();
		

    }


(function($) {
   
    
    init = function(){
        
		var successCallback = function (result) {
			//console.log(result);
			if(result == 1)
			{
				uniwarning('Peringatan','Device anda telah di root, Aplikasi tidak diijinkan digunakan!');
				localStorage.clear();
				navigator.app.exitApp();	  
			}
			else
			{
				//uniwarning('Peringatan','Device anda aman!');
			}
		
		};
		var errorCallback = function (error) {
			console.error(error);
		};
		rootdetection.isDeviceRooted(successCallback, errorCallback);
		
		
		
		SetItem('PageLoadOnBack', 'wallet.html');
      

		var current_height = $(window).height();    
		var key;
		$('.mobile-wrapper').css('height',current_height);  
		window.AppsVersionId = 'UNIKAS Wallet Apps Alpha VER 1';
		window.MasterCode = 'UNIKAS';
		SetItem('MasterCode',MasterCode);
		window.service_api =  'https://nobuepay.nobubank.com/v2/';
		SetItem('ServiceApi',btoa(service_api));
	
		var sessionId = null;
		if(GetItem('UnikasWalletApp')){
			sessionId = GetItem('UnikasWalletApp');   
		}
		else
		{
		SetItem('UnikasWalletApp',null);
		}
   
        
		var device = JSON.parse(GetItem('Device'));
		var sessionId = GetItem('UnikasWalletApp');
		var ServiceApi = atob(GetItem('ServiceApi'));
		var buffer_page = null;
	
	SetItem('LastAccessTime',$.now());
	
		
    check_state();
	$('#userid-field').focus();
    // get_Wallet_performance();
    if(GetItem('PhotoProfile') != null){
    	var picurl = atob(GetItem('PhotoProfile'));
    	$('.profile-avatar-image').attr('src',picurl);
    }
    else
    {
    	if(GetItem('UnikasWalletAppLogData') != null){
	    	var AppData = JSON.parse(atob(GetItem('UnikasWalletAppLogData')));
	    	if(AppData.Data.UrlImageProfile != null)
	    	{
	    	    $('.profile-avatar-image').attr('src',AppData.Data.UrlImageProfile);
	    	}
	    }
    }

   
 
   if (!sessionId) {

    SetItem('UnikasWalletApp', null);

	}
	  
        
    };
    
    
    // End Of INIT
	
    
	$body = $("body");
	ajaxStart = function(timeout) {
		$body.addClass("loading");
		setTimeout(function() {
			$body.removeClass("loading");
		}, timeout);
	}
	ajaxStop = function() {
		$body.removeClass("loading");
	}
		//validate_session();
	
	//setInterval(function(){
//		validate_session();
//	},300000);
   
   
    setInterval(function(){
		check_activity();
		
	}, 1000);
   

   var currentpage = getCurentFileName();
	
	function validate_session(){


   	 	var ServiceApi = 'https://nobuepay.nobubank.com/v2/';

   	 	if(currentpage != 'login.html'){
   	 		
		$.get(ServiceApi+'base/validate_session').done(function(data){
			if(data == 'INVALID'){
					uniwarning('Peringatan','Sesi koneksi Anda telah berakhir. Silahkan Login kembali!');
					//localStorage.clear();
					//goToPage('screen_lock.html');
			}
			
		}).fail(function(){
			
			uniwarning('Peringatan','Tidak dapat terhubung dengan server!');
		});

		}
		
	}

	screen_lock_funct = function(){
				
		
		   if(GetItem('UnikasWalletApp') != "null")
		   {
			
//localStorage.clear();
					//goToPage('login.html');
				//refresh_state();			
				//alert("GOTO screen_lock");							
				goToPage('screen_lock.html');
				//alert("GOTO screen_lock");	
		   }
		  /*
		    else
		   {
			   alert("lain");			  
		   }
			
		*/
	};
	
	check_activity = function(){
		var last_activity = GetItem('LastAccessTime');
		//var Walletdata = GetItem('UnikasWalletApp');
		if(currentpage == 'wallet.html')
		{
			var difftime = $.now() -  last_activity;		
			var timeout = Math.round(difftime/1000);
			//console.log(timeout);
			//alert('last_activity'+last_activity);
			//alert('difftime'+difftime);
			//alert('timeout'+timeout);
			if(timeout > 1800){
				
				//check_auth();
				goToPage('screen_lock.html');
				//alert('checkauth');
		//screen_lock_funct();
					

			}
			
			var networkState = navigator.connection.type;

			var states = {};
			states[Connection.UNKNOWN]  = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI]     = 'WiFi connection';
			states[Connection.CELL_2G]  = 'Cell 2G connection';
			states[Connection.CELL_3G]  = 'Cell 3G connection';
			states[Connection.CELL_4G]  = 'Cell 4G connection';
			states[Connection.CELL]     = 'Cell generic connection';
			states[Connection.NONE]     = 'No network connection';

			//alert('Connection type: ' + states[networkState]);

			if(states[networkState] == 'No network connection')
			{
								
				goToPage('no_connection.html');
			
			}
		}		
		else if(currentpage == 'no_connection.html')
		{	
			var networkState = navigator.connection.type;

			var states = {};
			states[Connection.UNKNOWN]  = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI]     = 'WiFi connection';
			states[Connection.CELL_2G]  = 'Cell 2G connection';
			states[Connection.CELL_3G]  = 'Cell 3G connection';
			states[Connection.CELL_4G]  = 'Cell 4G connection';
			states[Connection.CELL]     = 'Cell generic connection';
			states[Connection.NONE]     = 'No network connection';

			//alert('Connection type: ' + states[networkState]);

			if(states[networkState] != 'No network connection')
			{
								
				goToPage('wallet.html');
			
			}
		}
	};
	
	
   	refreshdata = function(pagetarget) {
          
       $.when(check_auth()).then(function(){
       	$.when(retrieve_Wallet_data()).then(function(){
       			if(pagetarget)
       			{
       				goToPage(pagetarget);
       			}       			
     	  	});       	
       });
    };
	
	
	CheckPassword = function (inputtxt) {
		//var paswd=/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,25}$/;
		//if(inputtxt.value.match(paswd))
			
		
		var e = String(inputtxt.value);
		var count1 = 0;
		var count2 = 0;
		var count3 = 0;
		
		if(inputtxt.value.length >= 6)
		{
			for(var x=0, y=1; x<=e.length, y<=e.length; x++, y++)
			{
				var z = parseInt(e[x])+1;
				var v = parseInt(e[x])-1;
					if(e[x] == e[y])
					{
					count1++;
						if(count1 == 3)
						{break;}
					}
					else
					{
					count1 = 0;
					}
					
					if(e[y] == z)
					{
					count2++;
						if(count2 == 3)
						{break;}
					}
					else 
					{
					count2 = 0;
					}
					
					 if(e[y] == v)
					{
					count3++;
						if(count3 == 3)
						{break;}
					}
					else 
					{
					count3 = 0;
					}    
			}
				
			if(count1 >= 3 || count2 >= 3 || count3 >= 3)
			{
				uniwarning('Informasi', 'PIN minimal 6 digit, tidak diperbolehkan deret angka secara berurutan, tidak diperbolehkan deret angka sama sebanyak 4 angka!');
				inputtxt.value = '';
				inputtxt.focus();		
			}	
		}
		else
		{
			uniwarning('Informasi', 'Password minimal 6 karakter!');
			inputtxt.value = '';
			inputtxt.focus();			
		}
	};
	
	CheckPin = function (inputtxt) {
		
		var e = String(inputtxt.value);
		var count1 = 0;
		var count2 = 0;
		var count3 = 0;
		var numbers = /^[0-9]+$/; 
		
		if((inputtxt.value.match(numbers)) && (inputtxt.value.length >= 6))
		{
			for(var x=0, y=1; x<=e.length, y<=e.length; x++, y++)
			{
				var z = parseInt(e[x])+1;
				var v = parseInt(e[x])-1;
					if(e[x] == e[y])
					{
					count1++;
						if(count1 == 3)
						{break;}
					}
					else
					{
					count1 = 0;
					}
					
					if(e[y] == z)
					{
					count2++;
						if(count2 == 3)
						{break;}
					}
					else 
					{
					count2 = 0;
					}
					
					 if(e[y] == v)
					{
					count3++;
						if(count3 == 3)
						{break;}
					}
					else 
					{
					count3 = 0;
					}    
			}
				
			if(count1 >= 3 || count2 >= 3 || count3 >= 3)
			{
				uniwarning('Informasi', 'PIN minimal 6 digit, tidak diperbolehkan deret angka secara berurutan, tidak diperbolehkan deret angka sama sebanyak 4 angka!');
				inputtxt.value = '';
				inputtxt.focus();		
			}			
		}
		else
		{
			uniwarning('Informasi', 'PIN minimal 6 digit dan dalam bentuk angka!');
			inputtxt.value = '';
			inputtxt.focus();		
		}
	};
	
	updatebalance = function() {
							
       $.when(check_auth()).then(function(){
		   retrieve_Wallet_data();
       });
    };
	
    check_auth = function(){
    	var param = {'data':encap(GetItem('CheckParam')),
                     'deviceid':GetItem('DeviceUUID')};
        
         var param_checkauth = JSON.stringify(param); 
					  
         $.post(service_api+'login/create_login',encap(param_checkauth)).done(function(data){
         	refresh_state();
            		////console.log(JSON.stringify(data));
			var logindata = JSON.parse(decap(data));
              //console.log(logindata);
			if(logindata.Status != 'Failed')
			{
				if (logindata.Data.KYCStatus == 1) {
					$('#kyc-image').empty().append('<span id="is-verified-member">&nbsp;<img style="margin-bottom: 5px;" src="images/wallet/verified_member.png" width="24" height="24" /></span>');
					$('.menu-check-kyc').removeClass('disabled_menu');
				} else {
					$('#is-verified-member').remove();
					$('.menu-check-kyc').addClass('disabled_menu');
				}
				
				$('#wallet_menu').show();
				////console.log("wallet_menu show");
                //break;
				SetItem('UserId',btoa(logindata.Data.UserId));
				SetItem('UnikasWalletApp',btoa(JSON.stringify(logindata.Data)));
				SetItem('WalletData',btoa(JSON.stringify(logindata.Data)));
				SetItem('UnikasWalletAppLogData',btoa(JSON.stringify(logindata)));
				SetItem('WalletUniqData',btoa(logindata.Data.EwalletUniqueCode));
				SetItem('ServiceApi',btoa(service_api));
				SetItem('AppsVersionId',AppsVersionId);
 		   		SetItem('MasterCode',MasterCode);
                
				
                
                
                socket.emit('send_notification',{
						"EWalletUniqueCode": logindata.Data.EwalletUniqueCode,
						"AccountType": logindata.Data.AccountType,
						"DeviceUUID": GetItem('DeviceUUID')
					});

 		   		if(logindata.Data.EwalletTypeAccountCode == 'MERCHANT') {
					merchantType = logindata.Data.EwalletTypeAccountCode;
                    $('#MenuTransMerchant').show();
 		   			merchant_check();                   
 		   		} else {
					$('#MenuTransCustomer').show();
				}
				
				
			}
			else
			{
				localStorage.clear();
				goToPage('login.html');
			}
			
		
		})
		.fail(function(data){
			//console.log(data);
		});
    };

	merchant_check = function(){
    	 var param = {'data':encap(GetItem('CheckParam')),
                      'deviceid':GetItem('DeviceUUID')};
        
         var param_merchantcheck = JSON.stringify(param); 
         $.post(service_api+'merchant/login/',encap(param_merchantcheck)).done(function(data){			
			refresh_state();
			var logindata = JSON.parse(decap(data));
				//console.log("merchant login check"+logindata);
			if(logindata.Status != 'Failed')
			{
				$('#wallet_menu').hide();
				$('#merchant_menu').show();
				$('.menu_merchant').show();
				$('.menu_akunsaya').show();
				SetItem('UnikasMerchantApp',btoa(JSON.stringify(logindata.Data)));
				SetItem('MerchantData',btoa(JSON.stringify(logindata.Data)));
				SetItem('UnikasMerchantAppLogData',btoa(JSON.stringify(logindata)));
				SetItem('MerchantUniqData',btoa(logindata.Data.MerchantCode));
			}
			
		
		})
		.fail(function(data){
			//console.log(data);
		});
    };


   	function getCurentFileName(){
		var pagePathName= window.location.pathname;
		return pagePathName.substring(pagePathName.lastIndexOf("/") + 1);
	}

	function getToday() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			var hour = today.getHours();
			var minute = today.getMinutes();
			var second = today.getSeconds();

			if(dd<10) {
				dd='0'+dd
			} 

			if(mm<10) {
				mm='0'+mm
			}

			if(hour<10) {
				hour='0'+hour;
			}
			if(minute<10) {
				minute='0'+minute;
			}
			if(second<10) {
				second='0'+second;
			}

			today = yyyy+mm+dd+hour+minute+second;
			return today;
	}
	

  login = function (login){
	 cordova.plugin.pDialog.init({
     theme : 'HOLO_DARK',
     progressStyle : 'SPINNER',
     cancelable : true,
     title : 'Silahkan Tunggu...',
     message : 'Masuk ...'
	 });
	
	var trxdate = getToday();
  	var loginparam = ''

  	if(GetItem('CheckParam'))
  	{
  		loginparam = encap(GetItem('CheckParam'));
  	}
  	else{

  		loginparam = btoa(JSON.stringify(login));
  	}


      
    
  	var paramdata = {'data':betoa(loginparam),
				 'device':document.cookie,
                 'deviceid':GetItem('DeviceUUID')
                 };
////console.log("paramdata"+JSON.stringify(paramdata));
	var param = JSON.stringify(paramdata); 
      //console.log("param"+param);
       //console.log("param"+encap(param));
      //console.log("param"+decap(encap(param))); 
 	$.post(service_api+'login/create_login',encap(param)).done(function(data){
			//alert(JSON.stringify(data));
			refresh_state();
        
           // //console.log(JSON.stringify(data));
			var logindata = JSON.parse(decap(data));
			//alert(JSON.stringify(data));
			if(logindata.Status != 'Failed')
			{	
				
				cordova.plugin.pDialog.dismiss();
				
				uniwarning('Langkah Login', logindata.Message);
							
					$('#login-form').hide();
					$('#login-formotp').show();

					var userid = $("#userid-field").val();
					
					var requestotpdata = {			                
						'Notes':  logindata.Data.EwalletUniqueCode+ '|LOGINOTP|'+trxdate
						};
                    
                    var paramdatas = {'data':betoa(btoa(JSON.stringify(requestotpdata))),
					 'device':document.cookie,
					 'deviceid':GetItem('DeviceUUID')
					 };
                    
	               var param2 = JSON.stringify(paramdatas); 
                                       	
					$.post(service_api+'transaction/generate_otptransaction', encap(param2)).done(function(data) {
						refresh_state();
						
						//var logindata = JSON.parse(decap(data));					
						
						/*
						if (logindata == "true") 
						{
					
						} 
						else {

						}
						*/
					});
					
					$("#kodeotp-field").focus();
					
				/*
					if (logindata.Data.IsHasPin == 0)
					{
						var Pin, Password;
						swal({
							title: " ",
							text: "Buat PIN untuk melakukan Transaksi :",
							type: "input",
							showCancelButton: false,
							closeOnConfirm: false,	
							inputPlaceholder: "Minimal 6 digit"
						},
						function(inputValue){
							if (inputValue === false) 
								return false;
							if (inputValue === "") 
							{
								swal.showInputError("PIN tidak boleh kosong!");
								return false;
							}
							
							Pin = inputValue;
							swal({
							title: " ",
							text: "Buat Password baru untuk Masuk :",
							type: "input",
							showCancelButton: false,
							closeOnConfirm: false,	
							inputPlaceholder: "Minimal 6 digit"
							},
							function(inputValue2){
								if (inputValue2 === false) return false;
								if (inputValue2 === "") {
									swal.showInputError("Password tidak boleh kosong!");
									return false;
								}
								
								Password = inputValue2;				
								
								var login = {'UserId':$("#userid-field").val(),
								'Password':Password};	
								
								var loginparam2 = btoa(JSON.stringify(login));								
								create_new_pin(logindata.Data.UserId,Pin,Password,loginparam2, logindata, service_api);
							});
						});
						
						/*
							navigator.notification.prompt(
								'Buat PIN sebelum melakukan transaksi :',  // message
								onPrompt,                  // callback to invoke
								'Kelengkapan Data',            // title
								['Ok'],             // buttonLabels
								null
							);
							
							*/
							/*
					}
					else 
					{
						initiate_local_data(loginparam, logindata, service_api);
					}	
					*/					
				
			}
			else
			{
				//alert("gagal");
				cordova.plugin.pDialog.dismiss();
				if(logindata.Data == 0)
				{	
					uniwarning('Login Gagal', logindata.Message);				
					$("#password-field").val('');
					$("#password-field").focus();
				}
				else
				{
					uniwarning('Login Gagal', logindata.Message);
					$("#userid-field").val('');
					$("#password-field").val('');
					$("#userid-field").focus();
				}
                
                refresh_state();
				
				
			}
			
		
		})
		.fail(function(data){
			cordova.plugin.pDialog.dismiss();
			alert("Gagal menyambungkan!");
			//console.log(data);
		});
  };
  
 loginandotp = function (loginandotp){
	
	 cordova.plugin.pDialog.init({
     theme : 'HOLO_DARK',
     progressStyle : 'SPINNER',
     cancelable : true,
     title : 'Silahkan Tunggu...',
     message : 'Masuk ...'
	 });
	
  	var loginparam = ''

  	if(GetItem('CheckParam'))
  	{
  		loginparam = encap(GetItem('CheckParam'));
  	}
  	else
	{
  		loginparam = btoa(JSON.stringify(loginandotp));
  	}

  	////console.log(loginparam);
  	var param = {'data':betoa(loginparam),
				 'device':document.cookie,
                 'deviceid':GetItem('DeviceUUID')};
	
    var paramloginandotp = JSON.stringify(param); 
  	$.post(service_api+'login/loginandotp',encap(paramloginandotp)).done(function(data){
			
  			refresh_state();
			var logindata = JSON.parse(decap(data));
			if(logindata.Status == 'Success')
			{	
				
				
				cordova.plugin.pDialog.dismiss();
				
				//
				if (logindata.Data.IsHasPin == 0)
					{
						var Pin, Password;
						swal({
							title: " ",
							text: "Buat PIN untuk melakukan Transaksi :",
							type: "input",
							showCancelButton: false,
							closeOnConfirm: false,	
							inputPlaceholder: "Minimal 6 digit"
						},
						function(inputValue){
							if (inputValue === false) 
								return false;
							if (inputValue === "") 
							{
								swal.showInputError("PIN tidak boleh kosong!");
								return false;
							}
							
							Pin = inputValue;
							swal({
							title: " ",
							text: "Buat Password baru untuk Masuk :",
							type: "input",
							showCancelButton: false,
							closeOnConfirm: false,	
							inputPlaceholder: "Minimal 6 digit"
							},
							function(inputValue2){
								if (inputValue2 === false) return false;
								if (inputValue2 === "") {
									swal.showInputError("Password tidak boleh kosong!");
									return false;
								}
								
								Password = inputValue2;				
								
								var login = {'UserId':$("#userid-field").val(),
								'Password':Password};	
								
								var loginparam2 = btoa(JSON.stringify(login));								
								create_new_pin(logindata.Data.UserId,Pin,Password,loginparam2, logindata, service_api);
							});
						});																											
					}
					else 
					{
						uniwarning('Notifikasi', 'Untuk sementara, transaksi Nobu ePay tidak dapat diproses sehubungan periode testing telah selesai. Kami akan informasikan saat aplikasi Nobu ePay diaktifkan kembali. Mohon maaf atas ketidaknyamanannya, terima kasih.');
						initiate_local_data(loginparam, logindata, service_api);
					}	
										
				
			}
			else if(logindata.Status == 'Failed')
			{	cordova.plugin.pDialog.dismiss();
				uniwarning('Login Gagal', logindata.Message);
				$("#kodeotp-field").val('');
				$("#kodeotp-field").focus();
			}
            else 
			{	cordova.plugin.pDialog.dismiss();
				uniwarning('Login Gagal', logindata.Message);
				goToPage('login.html');
			}
			
		
		})
		.fail(function(data){
			cordova.plugin.pDialog.dismiss();
			alert("Gagal menyambungkan!");
			//console.log(data);
		});
  };
 
 
 loginpin = function (loginpin){
	
	//Refresh Session	
	
	 cordova.plugin.pDialog.init({
     theme : 'HOLO_DARK',
     progressStyle : 'SPINNER',
     cancelable : true,
     title : 'Silahkan Tunggu...',
     message : 'Masuk ...'
	 });
	
	
	if(GetItem('UnikasWalletAppLogData') != null)
	{
				var AppData = JSON.parse(atob(GetItem('UnikasWalletAppLogData')));
				if(AppData.Data.UrlImageProfile != null)
				{
				//uniwarning('Peringatan',AppData.Data.UserId);
				
				var logindata = {'UserId':AppData.Data.UserId,
								 'Pin':loginpin
								 };	   
				    
				var paramdata = {'data':betoa(btoa(JSON.stringify(logindata))),
				 'device':document.cookie,
                 'deviceid':GetItem('DeviceUUID')
                 };
				
				var param = JSON.stringify(paramdata); 
				//uniwarning('Informasi', param);
				 	$.post(service_api+'login/loginpin',encap(param)).done(function(data){		
					//uniwarning('Informasi','s');					
					refresh_state();
					
						var logindata2 = JSON.parse(decap(data));
						//uniwarning('Informasi login', JSON.stringify(logindata2));
						if(logindata2.Status == 'Success')
						{	
											
							var loginparam = ''

							if(GetItem('CheckParam'))
							{
								loginparam = GetItem('CheckParam');
							}
														 						
							var paramdata2 = {'data':betoa(loginparam),
										 'device':document.cookie,
										 'deviceid':GetItem('DeviceUUID')
										 };
						
							var param2 = JSON.stringify(paramdata2); 
							//uniwarning('param2',param2);
							$.post(service_api+'login/create_login',encap(param2)).done(function(data){								
									refresh_state();
									cordova.plugin.pDialog.dismiss();									
									var logindata = JSON.parse(decap(data));	
									//uniwarning('Informasi login pin', JSON.stringify(logindata));
									if(logindata.Status != 'Failed')
									{										
										initiate_local_data_forpinlogin(loginparam, logindata, service_api);
										goToPage('wallet.html');
									}
									else
									{
										goToPage('screen_lock.html');
									}
							});
							/*
							cordova.plugin.pDialog.dismiss();							
							goToPage('wallet.html');
							check_auth();
							*/
						}
						else
						{	
							uniwarning("Informasi", "PIN yang Anda masukan salah!");
							cordova.plugin.pDialog.dismiss();
							//uniwarning('Informasi Login', logindata.Message);
							goToPage('screen_lock.html');
						}
						
					});
				}
	}
	else
	{
		cordova.plugin.pDialog.dismiss();
		goToPage('login.html');
	}
/*
  	////console.log(loginparam);
  	var param = {'data':betoa(loginparam),
				 'device':document.cookie,
                 'deviceid':GetItem('DeviceUUID')};
	
    var paramloginandotp = JSON.stringify(param); 
  	$.post(service_api+'login/loginpin',encap(paramloginandotp)).done(function(data){
			
  			refresh_state();
			cordova.plugin.pDialog.dismiss();
			var logindata = JSON.parse(decap(data));
			if(logindata.Status == 'Success')
			{	
				
				
				uniwarning('Login Berhasil', logindata.Message);
				
			}	
            else 
			{	
				uniwarning('Login Gagal', logindata.Message);
				//goToPage('login.html');
			}
			
		
		})
		.fail(function(data){
			cordova.plugin.pDialog.dismiss();
			alert("Gagal menyambungkan!");
			//console.log(data);
		});
		*/
  };
 //start region set pin first time 
 
 initiate_local_data = function(loginparam, logindata, service_api) {
	SetItem('CheckParam',loginparam);
	SetItem('CheckParam2',loginparam);
	SetItem('UserId',btoa(logindata.Data.UserId));
	SetItem('UnikasWalletApp',btoa(JSON.stringify(logindata.Data)));
	SetItem('WalletData',btoa(JSON.stringify(logindata.Data)));
	SetItem('UnikasWalletAppLogData',btoa(JSON.stringify(logindata)));
	SetItem('WalletUniqData',btoa(logindata.Data.WalletCode));
	SetItem('ServiceApi',btoa(service_api));
	SetItem('AppsVersionId',AppsVersionId);
	SetItem('MasterCode',MasterCode);
	SetItem('IsWaitingApproval',logindata.Data.IsWaitingApproval);
	SetItem('ButtonLoadPage', null);
	SetItem('ButtonLoadPageMarshmallow', null);
	SetItem('AppsClose', null);
	check_state();
  }
  
   initiate_local_data_forpinlogin = function(loginparam, logindata, service_api) {
	SetItem('CheckParam',loginparam);
	SetItem('CheckParam2',loginparam);
	SetItem('UserId',btoa(logindata.Data.UserId));
	SetItem('UnikasWalletApp',btoa(JSON.stringify(logindata.Data)));
	SetItem('WalletData',btoa(JSON.stringify(logindata.Data)));
	SetItem('UnikasWalletAppLogData',btoa(JSON.stringify(logindata)));
	SetItem('WalletUniqData',btoa(logindata.Data.WalletCode));
	SetItem('ServiceApi',btoa(service_api));
	SetItem('AppsVersionId',AppsVersionId);
	SetItem('MasterCode',MasterCode);
	SetItem('IsWaitingApproval',logindata.Data.IsWaitingApproval);
	SetItem('ButtonLoadPage', null);
	SetItem('ButtonLoadPageMarshmallow', null);
	SetItem('AppsClose', null);
  }
  
  
  create_new_pin = function(userid,pin,password,loginparam, logindata, service_api) {
	var par = {'data':betoa(btoa(JSON.stringify({'UserId':userid,'PIN':pin, 'Password': password}))),
                'deviceid':GetItem('DeviceUUID')
                            
                            };
	$.post(service_api+'merchant/mer_set_pin/',encap(JSON.stringify(par))).done(function(data){
		 refresh_state();
		 var res = JSON.parse(decap(data));
		 if (res.Status == 'Success') {
			swal({
				title: "Informasi",
				text: "Terima Kasih, PIN dan Password anda sekarang sudah aktif!",
				// closeOnConfirm: false,
				confirmButtonText: "OK"
			},
			function(){
				initiate_local_data(loginparam, logindata, service_api);
			});
		 } else {
			 swal("Informasi", res.Message);
		 }
	}).fail(function(data){
		//console.log(data);
	});
  }
 
 
 //end region set pin first time
 
 
 
 
 


    check_state = function (){
	var userid = GetItem('UserId');
		
	getPosition();
	 
		
		if(!GetItem('UnikasLocationData'))
		{
			
			getPosition();
		
		}
		
		if(userid == null){
		
			if(currentpage == 'register.html'){
					
					return false;

			}
			
			
			if(currentpage == 'wallet.html'){
			
					goToPage('login.html');

			}					
			
		}
		else
		{
			
			if(currentpage == 'login.html')
				{
				
					if(userid != null)
					{																							
						//refreshdata();
						goToPage('wallet.html');
						
					}
					else{
					
						localStorage.clear();
					}
				}
		/*	else if(currentpage == 'screen_lock.html')
				{
				
					if(userid != null)
					{																							
						//refreshdata();
						goToPage('wallet.html');
						
					}
					else{
					
						localStorage.clear();
					}
				}		*/	
			else if(currentpage == 'wallet.html')
				{
				
					$('#add-button').hide();
					refreshdata();
					// retrieve_Wallet_data();
				}
		}
		

		
		
	}; 	
	
	getPosition = function() {
		//Menandai agar saat pause app tidak terlock
		//SetItem('ButtonLoadPage', 'pluginactive');
		//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
		

	   var options = {
		  enableHighAccuracy: true,
		  maximumAge: 3600000
	   }
		
	   var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

	   function onSuccess(position) {
		      
		   var locationdata = {'Latitude':position.coords.latitude,
							   'Longitude':position.coords.longitude,
							   'Accuracy':position.coords.accuracy,   
							   'Timestamp':position.timestamp};
		  
		
		   SetItem('LocationLatitude',position.coords.latitude);
		   SetItem('LocationLongitude',position.coords.longitude);
		   SetItem('UnikasLocationData',JSON.stringify(locationdata));
			//alert(JSON.stringify(locationdata));
			return locationdata;

	
	   };

	   function onError(error) {
		   ////console.log(error);
		  
			function onDeviceReady() 
			{
				
				//if(cordova.plugins.backgroundMode.isActive()==false)
				//{			
			
				//SetItem('ButtonLoadPage', 'pluginactive');
				//SetItem('ButtonLoadPageMarshmallow', 'pluginactive');
		
				//cordova.plugins.backgroundMode.setDefaults({ silent: true });
				//cordova.plugins.backgroundMode.setEnabled(true);					
				
				cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
					for (var permission in statuses){
						switch(statuses[permission]){
							case cordova.plugins.diagnostic.permissionStatus.GRANTED:
								//console.log("Permission granted to use "+permission);
								break;
							case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
								//console.log("Permission to use "+permission+" has not been requested yet");
								break;
							case cordova.plugins.diagnostic.permissionStatus.DENIED:
								//console.log("Permission denied to use "+permission+" - ask again?");
								break;
							case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
								//console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
								break;
						}
					}
				}, function(error){
					console.error("The following error occurred: "+error);
				},[
					cordova.plugins.diagnostic.permission.LOCATION
				]);														
				//}			
			}
		
		  return false;
	   }
	};

// agent apps function
	retrieve_Wallet_data = function(){
		
		if(GetItem('UnikasWalletAppLogData'))
		{
			var userdata = JSON.parse(atob(GetItem('UnikasWalletAppLogData')));			
			$('.profile-avatar-name').text(userdata.Data.FirstName + ' ' + userdata.Data.MiddleName + ' ' + userdata.Data.LastName);
			$('.profile-avatar-job').text(userdata.Data.UserId);
			$('#saldo_total').text('Rp. '+userdata.Data.CurrentBalance);
			$('#point_total').text(userdata.Data.CurrentPoint);
		}
	};
	

	formatCurrency = function(total) {
	    var neg = false;
	    if(total < 0) {
	        neg = true;
	        total = Math.abs(total);
	    }
	    return (neg ? "-Rp. " : 'Rp. ') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").replace('.00','').toString();
	};


	clearapp = function(){
		localStorage.clear();
	};

	goToPage = function(hrefPage){
			document.location = hrefPage;
		};


	load_page = function(that){
		if (isHasClass(that.attr('class'),'disabled_menu')) {
			uniwarning('Informasi', 'Maaf, anda belum bisa menggunakan layanan ini. Silahkan tingkatkan fasilitas keanggotaan akun anda!');
		} else {
			var page = that.attr('page-load');
			if (page == 'wallet/profile') {
			var IsWaitingApproval = GetItem('IsWaitingApproval');
			//console.log(IsWaitingApproval);
				/*if (IsWaitingApproval != null && IsWaitingApproval == 1) {
					unialertsuccess('Informasi', 'Kelengkapan Data untuk GOLD Member sudah kami terima. Proses Persetujuan membutuhkan waktu 2 - 3 hari kerja. Terima kasih.');
				}*/
			}
			
			
			$('#add-button').show();
				$('.add-button').attr('page-load',that.next().attr('page-load'));
				$('.add-button').attr('page-title',that.next().attr('page-title'));
				
				
				
				var title = that.attr('page-title');
				$(".navbar-title").text(title);
				$(".navbar-title").removeClass("italic");
				var page_api = service_api+'base/page/?type='+page;
				$('.loading-mask').removeClass('stop-loading');
				$.get(page_api).done(function(data){
				$(window).scrollTop(0);
				$('.loading-mask').addClass('stop-loading');
				$('.body').empty().append(data);

				}).fail(function(){
				$('.loading-mask').addClass('stop-loading');
				uniwarning('Information', 'Communication to server fail');
				});
			
			
			
			
		}
	};
	
	isHasClass = function(classList, nameOfClass) {
		if (classList == null) return false;
		return (classList).indexOf(nameOfClass) > -1; //indexOf > 0 true, -1 false
	}


	capturePhoto = function(){

	    var capture = navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 80,
        destinationType: destinationType.DATA_URL });

	  
	};

    capturePhotoEdit = function() {
      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,  targetWidth: 500,
    targetHeight: 500,popoverOptions: CameraPopoverOptions,
        destinationType: destinationType.DATA_URL });
    };

	getPhoto = function(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
    };




    function onPhotoDataSuccess(imageData) {
      // Uncomment to view the base64-encoded image data

       var postdata = btoa(JSON.stringify({'Image':imageData}));
		 $.post(service_api+'base/upload',encap({'data':postdata})).done(function(data){
			 refresh_state();
			 var response = jQuery.parseJSON(atob(data));
		
			 if(response.Status != 'Failed')
				{
     				SetItem('TempPictureUrl',response.Data);
     				return response;
     				
				}
					
			else{
				alert(response.Status);
			}
			 
			
		 }).fail(function(){
			 alert('Upload Data Gagal');
			 
		 });

    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
      // Uncomment to view the image file URI
      // //console.log(imageURI);

      // Get image handle
      //
      var largeImage = document.getElementById('largeImage');

      // Unhide image elements
      //
      largeImage.style.display = 'block';

      // Show the captured photo
      // The in-line CSS rules are used to resize the image
      //
      largeImage.src = imageURI;

       return smallImage.src;
    }



   function onFail(message) {
      alert('Failed because: ' + message);
    };



    scanqrcode = function()
		{
			var text;
		    cordova.plugins.barcodeScanner.scan(
		        function (result) {
		            if(!result.cancelled)
		            {
		                return result;
		            }
		        },
		        function (error) {
		            alert("Scanning failed: " + error);
		        }
		   );
		};

		
		// scroll detection 
/*
   var position = $(window).scrollTop();

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll > position) {
								
				if(merchantType != null)
				{
					$('#merchant_menu').slideUp();	
				}
				else
				{
					$('#wallet_menu').slideUp();	
				}
				

        } else {
				if(merchantType != null)
				{
					$('#merchant_menu').slideDown();
				}
				else
				{
					$('#wallet_menu').slideDown();
				}
        }

        position = scroll;
    });
	*/
	function alertDismissed() {
		$("#btnHistory" ).trigger( "click" );
		updatebalance();
	}

	
	
	unialert = function(title, message) {
		    navigator.notification.alert(
			    message, // message
			    alertDismissed, // callback
			    title, // title
			    'OK' // buttonName
		    );
	}
	
	unialertsuccess = function(title, message) {
		navigator.notification.alert(
			message, // message
			function() {
				$("#btnHome" ).trigger( "click" );
			}, // callback
			title, // title
			'OK' // buttonName
		);
	}
	
	unialertfail = function(message) {
		navigator.notification.alert(message);
	}
	
	uniwarning = function(title, message) {
		  navigator.notification.alert(
			  message,
			  function(){},
			  title,
			  'OK'
		  );
	}
	
	getTransactionDate = function() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var hour = today.getHours();
		var minute = today.getMinutes();
		var second = today.getSeconds();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		}
		if(hour<10) {
			hour='0'+hour;
		}
		if(minute<10) {
			minute='0'+minute;
		}
		if(second<10) {
			second='0'+second;
		}
		return dd+'/'+mm+'/'+yyyy+' '+hour+':'+minute+':'+second;
	}
	
	isValidDate = function(inputText) {
		var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;  
		if(inputText.val().match(dateformat)) {
			var opera1 = inputText.val().split('/');  
			var opera2 = inputText.val().split('-');  
			lopera1 = opera1.length;  
			lopera2 = opera2.length;  
			
			if (lopera1>1) {  
				var pdate = inputText.val().split('/');  
			} else if (lopera2>1) {  
				var pdate = inputText.val().split('-');  
			}  
			var dd = parseInt(pdate[0]);  
			var mm  = parseInt(pdate[1]);  
			var yy = parseInt(pdate[2]);
			
			if (new Date() < new Date(yy,(mm-1),dd)) {
				uniwarning(' ', 'Tanggal tidak boleh lebih dari hari ini');
				inputText.focus();
				return false;  
			}
			
			var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
			var lMonthName = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
			if (mm==1 || mm>2) {  
				if (dd>ListofDays[mm-1]) {  
					uniwarning(' ', 'Tanggal tidak valid');
					inputText.focus();
					return false;  
				}
			}
			if (mm==2) {
				var lyear = false;  
				if ( (!(yy % 4) && yy % 100) || !(yy % 400)) {  
					lyear = true;  
				}
				if (lyear && dd>29) {  
					uniwarning(' ', 'Tanggal tidak valid');
					inputText.focus();					
					return false;  
				}
				if (!lyear && dd>28) {  
					uniwarning(' ', 'Tanggal tidak valid');
					inputText.focus();
					return false;  
				}  
			}  
		} else {  
			uniwarning(' ', 'Format tanggal salah');
			inputText.focus();
			return false;  
		}
	}
	
	getDateFormat = function(param) {
		if (param.length == 10 && param.indexOf("-") != -1) {
			var res = param.split("-");
			return res[2] + "/" + res[1] + "/" + res[0];
		}
		return "00/00/0000";
	}


}(jQuery));